# Audio Visualizer

A basic audio spectrum visualizer CLI on python.

## Requirements

* numpy
* scipy

## Usage

```
$ python audio_visualizer.py filename.wav
```

## Features 

[x] Handle mono and stereo WAV files.
[] Handle mp3 files.

## Credits

All demo files on fixtures is only for demo.
