#!/usr/bin/python
import time
import os
import sys
import math
import wave
import numpy
import sounddevice as sd
import soundfile as sf

from random import seed
from random import randint
from scipy.fftpack import dct

seed(1)
os.system('clear')

file_name = sys.argv[1]

screen_width, screen_height = os.get_terminal_size()
max_screen_height = screen_height - 3
max_screen_width = screen_width - 3
mi_screen_width = int(max_screen_width/2)
qu_screen_width = int(max_screen_width/4)

sound_data, fs = sf.read(file_name, dtype='float32')

refresh = 10
block_data = int(fs/refresh)
block_data_step = 1
time_sleep = 1/refresh

time_sound = round(len(sound_data)/fs,2)

start = time.time()
sd.play(sound_data, fs)
with sf.SoundFile(file_name, 'r+') as f:
	while f.tell() < f.frames:
		start_frame = time.time()
		pos = f.tell()
		
		pourcentage = (pos/f.frames)
		pourcentage_time = (time.time()-start)/time_sound

		data = f.read(int(block_data))

		os.system('clear')
		print("{} - {} fps - {}x{} - {}% {}% {}".format(file_name, fs, max_screen_width, max_screen_height, round(pourcentage*100,2), round(pourcentage_time*100,2), (pourcentage_time-pourcentage)*100))
		print("{}/{} {}".format(round(time.time() - start, 2), time_sound, "."*int(pourcentage_time*(max_screen_width - 15))))
		
		for tab in range(max_screen_height):
			tab_index = int(tab * block_data_step)
			if (isinstance(data[tab_index], float)):
				tab_0 = data[tab_index]*mi_screen_width
				tab_0_dot = int(mi_screen_width+tab_0)
				print("{}".format("."*tab_0_dot))
			else:
				tab_0 = data[tab_index][0]*qu_screen_width
				tab_1 = data[tab_index][1]*qu_screen_width
				tab_0_dot = int(qu_screen_width+tab_0)
				tab_1_dot = int(qu_screen_width+tab_1)
				space_dot = max_screen_width-tab_0_dot-tab_1_dot
				print("{}{}{}".format("."*tab_0_dot ," "*space_dot ,"."*tab_1_dot))

		time.sleep(time_sleep - (time.time()-start_frame) - 0.0003)

os.system('clear')